package com.anahoret.annotations.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by heinr on 23.01.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Singleton {
}
