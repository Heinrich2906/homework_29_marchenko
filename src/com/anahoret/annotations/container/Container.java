package com.anahoret.annotations.container;

import com.anahoret.annotations.annotations.Autowired;
import com.anahoret.annotations.annotations.Component;
import com.anahoret.annotations.annotations.Singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Container {

    static Map<Class, Object> classMap = new HashMap<>();

    public static Object getBean(Class<?> clazz) {
        try {
            Constructor constructor = clazz.getConstructor();
            Object instance = classMap.get(clazz);
            if (instance != null) {
                return instance;
            }
            instance = constructor.newInstance();

            List<String> arrayList = new ArrayList<>();

            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                Autowired autowired = method.getAnnotation(Autowired.class);
                if (autowired != null && method.getName().startsWith("set")) {
                    Class[] paramTypes = method.getParameterTypes();
                    String fieldName = method.getName();
                    if (paramTypes.length > 1) {
                        System.out.println("Method " + fieldName.toString() + " has more then one parametres ((");
                    } else {
                        boolean oldAccesible = method.isAccessible();
                        if (!oldAccesible) {
                            method.setAccessible(true);
                        }
                        Component component = (Component) paramTypes[0].getAnnotation(Component.class);
                        if (component != null) {
                            Singleton singleton = (Singleton) paramTypes[0].getAnnotation(Singleton.class);
                            Object newClass = null;
                            if (singleton != null) {
                                newClass = getBean(clazz);
                            } else {
                                newClass = paramTypes[0].newInstance();
                            }
                            method.invoke(instance, newClass);
                            method.setAccessible(oldAccesible);

                            String capitalLetter = fieldName.substring(3, 4).toLowerCase();
                            fieldName = fieldName.substring(4);
                            arrayList.add(capitalLetter + fieldName);
                        }
                    }
                }
            }

            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                Autowired autowired = field.getAnnotation(Autowired.class);
                if (autowired != null) {

                    String fieldName = field.getName();
                    boolean weHaveSuchMethod = false;

                    for (String s : arrayList) {
                        if (s.equals(fieldName)) {
                            weHaveSuchMethod = true;
                            break;
                        }
                    }
                    if (!weHaveSuchMethod) {
                        Object fieldValue = getBean(field.getType());
                        boolean oldAccesible = field.isAccessible();
                        field.setAccessible(true);
                        field.set(instance, fieldValue);
                        field.setAccessible(oldAccesible);
                    }
                }
            }

            return instance;
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }
        return null;
    }
}
